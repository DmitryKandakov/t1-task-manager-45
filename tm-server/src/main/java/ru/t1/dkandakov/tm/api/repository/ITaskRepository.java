package ru.t1.dkandakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, created, name, description, status, user_id, project_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId});")
    void add(@NotNull TaskDTO model);

    @Nullable
    @Select("SELECT * FROM tm_task;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "description", column = "description")
    })
    List<TaskDTO> findAll();

    @Nullable
    @Select("SELECT * FROM tm_task ORDER BY name;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByName();

    @Nullable
    @Select("SELECT * FROM tm_task ORDER BY created;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByCreated();

    @Nullable
    @Select("SELECT * FROM tm_task ORDER BY status;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByStatus();

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllByUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByNameByUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByCreatedByUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY status;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByStatusByUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * from tm_task WHERE id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    TaskDTO findOneById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT * from tm_task WHERE id = #{id} AND user_id = #{userId} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    TaskDTO findOneByIdByUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT * from tm_task WHERE user_id = #{userId} AND project_id = #{projectId};")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllByProjectId(@Param("userId") @Nullable String userId, @Param("projectId") @Nullable String projectId);

    @Select("SELECT COUNT(1) FROM tm_task WHERE USER_ID = #{userId}")
    int getSizeByUserId(@NotNull String userId);

    @Select("SELECT COUNT(1) FROM tm_task")
    int getSize();

    @Delete("DELETE FROM tm_task WHERE id = #{id} and user_id = #{userId};")
    void removeOne(@Param("userId") @Nullable String userId, @Nullable TaskDTO model);

    @Delete("DELETE FROM tm_task WHERE id = #{id} and user_id = #{userId};")
    void removeOneByIdByUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Delete("DELETE FROM tm_task WHERE project_id = #{projectId};")
    void removeTasksByProjectId(@Param("projectId") @NotNull String projectId);

    @Delete("DELETE FROM tm_task WHERE id = #{id} and user_id = #{userId};")
    void remove(@Param("userId") @Nullable String userId, @Nullable TaskDTO model);

    @Update("UPDATE tm_task SET created = #{created}, name = #{name}, description = #{description}, status = #{status}, user_id = #{userId}, project_id = #{projectId} WHERE id = #{id};")
    void update(@NotNull TaskDTO model);

    @Delete("TRUNCATE TABLE tm_task;")
    void removeAll();

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void removeOneById(@NotNull String id);

}