package ru.t1.dkandakov.tm.api.service.dto;

import ru.t1.dkandakov.tm.dto.model.SessionDTO;

public interface ISessionServiceDTO extends IUserOwnedServiceDTO<SessionDTO> {
}
