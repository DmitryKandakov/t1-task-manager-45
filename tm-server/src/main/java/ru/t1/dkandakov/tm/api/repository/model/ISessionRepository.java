package ru.t1.dkandakov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.model.Session;

public interface ISessionRepository extends IUserOwnerRepository<Session> {

    void removeAll();

    void removeAll(@NotNull String userId);

}
