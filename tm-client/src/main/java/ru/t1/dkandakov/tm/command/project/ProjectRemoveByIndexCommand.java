package ru.t1.dkandakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;
import ru.t1.dkandakov.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.t1.dkandakov.tm.dto.response.project.ProjectRemoveByIndexResponse;
import ru.t1.dkandakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request =
                new ProjectRemoveByIndexRequest(getToken(), index);
        @NotNull final ProjectRemoveByIndexResponse response = getProjectEndpoint().removeProjectByIndex(request);
        @Nullable final ProjectDTO project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

}