package ru.t1.dkandakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.UserDTO;

public interface IUserRepository extends IRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

}





