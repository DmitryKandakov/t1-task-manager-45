package ru.t1.dkandakov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.dkandakov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

}
