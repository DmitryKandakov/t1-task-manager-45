package ru.t1.dkandakov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractIndexRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectGetByIndexRequest extends AbstractIndexRequest {

    public ProjectGetByIndexRequest(@Nullable String token, @Nullable Integer index) {
        super(token, index);
    }

}