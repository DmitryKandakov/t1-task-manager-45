package ru.t1.dkandakov.tm.exception.field;

public class UserIdEmptyException extends AbstractFildException {

    public UserIdEmptyException() {
        super("Error! User Id not found...");
    }

}
